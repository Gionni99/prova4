package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

/**
 * Linguagem Java
 * @author
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        this.reader=new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        this.reader=new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
        return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) throws NumberFormatException{
        Lancamento l = null;
        l.setConta(Integer.getInteger(linha.substring(0, 5)));
        l.setData(Date.valueOf(linha.substring(6, 13)));
        l.setDescricao(linha.substring(14, 73));
        l.setValor(Double.valueOf(linha.substring(74, 85)));
        return l;
    }
    
    private Lancamento getNextLancamento() throws IOException {
        Lancamento l1;
        l1=processaLinha(getNextLine());
        return l1;
    }
    
    public List<Lancamento> getLancamentos() throws IOException,NullPointerException {
        String linha;
        LancamentoComparator compara = null;
        List<Lancamento> lista=null;
        while((linha = getNextLine())!= null)
        {
            Lancamento l=null;
            l=processaLinha(linha);
            lista.add(l);
        }
        reader.close();
        lista.sort(compara);
        return lista;
    }
    
}
