package utfpr.ct.dainf.pratica;

import java.util.Comparator;
import java.util.Objects;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparator<Lancamento> {

    @Override
    public int compare(Lancamento t, Lancamento t1) {
        if(!Objects.equals(t.getConta(), t1.getConta()))
        {
            return t.getConta()-t1.getConta();
        }
        else
        {
            return t.getData().compareTo(t1.getData());
        }
    }

  
}
