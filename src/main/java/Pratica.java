
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Scanner scanner = new Scanner(System.in);
        String path;
        System.out.print("Digite o caminho para o arquivo: ");
        path=scanner.next();
        ProcessaLancamentos processador = new ProcessaLancamentos(path);
        List<Lancamento> lista;
        lista=processador.getLancamentos();
        Integer conta=1;
        String digitado;
        while(conta!=0)
        {
            System.out.print("Digite o numero de uma conta: ");
            digitado=scanner.next();
            conta=Integer.getInteger(digitado);
            if(conta==0)
                break;
            exibeLancamentosConta(lista,conta);
        }
        
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        Lancamento l=null;
        int i;
        for(i=0;i<lancamentos.size();i++)
        {
            if(lancamentos.get(i).getConta().equals(conta))
            {
                System.out.println(lancamentos.get(i).toString());
            }
        }
    }
 
}